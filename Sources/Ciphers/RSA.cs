﻿using System.IO;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;

namespace Iodynis.Libraries.Encryption
{
    /// <summary>
    /// RSA encryption.
    /// </summary>
    public static class RSA
    {
        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, byte[] key)
        {
            RsaEngine rsaEngine = new RsaEngine();
            RsaKeyParameters keyPrivate = PemToRsaKeyParameters(key);
            rsaEngine.Init(true, keyPrivate);

            byte[] bytes = rsaEngine.ProcessBlock(data, 0, data.Length);

            return bytes;
        }

        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, byte[] key)
        {
            RsaEngine rsaEngine = new RsaEngine();
            AsymmetricKeyParameter keyPublic = PemToRsaKeyParameters(key);
            rsaEngine.Init(false, keyPublic);

            byte[] bytes = rsaEngine.ProcessBlock(data, 0, data.Length);
            return bytes;
        }

        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Encrypt(data, keyBytes);
        }
        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Decrypt(data, keyBytes);
        }

        private static RsaKeyParameters PemToRsaKeyParameters(byte[] key)
        {
            RsaKeyParameters rsaKeyParameters;
            using (MemoryStream memoryStream = new MemoryStream(key))
            {
                using (StreamReader streamReader = new StreamReader(memoryStream))
                {
                    PemReader pemReader = new PemReader(streamReader);
                    rsaKeyParameters = (RsaKeyParameters)pemReader.ReadObject();
                }
            }
            return rsaKeyParameters;
        }
        //private static RsaKeyParameters PemToRsaKeyParameters(string param_pem)
        //{
        //    byte[] pem = Encoding.ASCII.GetBytes(param_pem);
        //    return PemToRsaKeyParameters(pem);
        //}
    }
}
